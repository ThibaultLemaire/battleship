# Battleship

A crazy school project about Battleship.

---

⚠ **ATTENTION** : This is a school project hosted here for demonstration purposes only. It is not fit for any particular purpose.

---

# Installation

`cd` into the cloned folder and install all the required project packages :

	npm install

Install two global packages :

	npm install -g supervisor
	npm install -g @angular/cli

Compile your files :

	ng build

- Create a `Battleship` database in MongoDB
- Be sure it is running on port `27017` or change the port in `server.js`

Run the following command to start the Node.js server :

    npm start

Access your app from `http://localhost:port` (Default is `3000` | Defined in `server.js`)

Each time you make a modification, compile your files again :

	ng build