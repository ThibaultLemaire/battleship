import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { RegisterComponent } from './register.component';
import { LoginComponent } from './login.component';
import { LobbyComponent } from './lobby.component';
import { PreGameComponent } from "./game/pregame.component";
import { EditProfileComponent } from "./edit-profile.component";
import { MainPageComponent } from "./main-page.component";
import { GameComponent } from "./game/game.component";

const appRoutes: Routes = [
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'lobby', component: LobbyComponent},
  { path: 'pregame', component: PreGameComponent},
  { path: 'edit-profile', component: EditProfileComponent},
  { path: 'pregame', component: PreGameComponent},
  { path: 'game', component: GameComponent},
  { path: '',
    pathMatch: 'full',
    component: MainPageComponent
  },
  {path: '**', redirectTo: ''}
];
  /*{ path: 'hero/:id',      component: HeroDetailComponent },
  {
    path: 'heroes',
    component: HeroListComponent,
    data: { title: 'Heroes List' }
  },
  { path: '',
    redirectTo: '/heroes',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];*/

@NgModule({
  declarations: [
    RegisterComponent,
    AppComponent,
    LoginComponent,
    LobbyComponent,
    PreGameComponent,
    EditProfileComponent,
    MainPageComponent,
    GameComponent
  ],
  imports: [
	HttpModule,
    JsonpModule,
  	FormsModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
