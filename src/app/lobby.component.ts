import { Component, OnDestroy, HostListener } from '@angular/core';
import * as io from "socket.io-client";
import { Service } from './service';

@Component({
  selector: 'lobby',
  template: `
    <div class="container">
    	<h1>Lobby</h1>
      <button type="button" class="btn btn-default" routerLink="/pregame" routerLinkActive="active">Play</button>
      <div class="row">
        <div class="col-md-6">
          <div id="chat">
            <h2>Chat</h2>
            <div id="messages"></div>
            <div style="padding-left: 20px;padding-right:20px;padding-top:15px;"class="form-group">
              <label for="msg">Message :</label>
              <input type="text" class="form-control" #msg id="msg" />
              <div style="text-align: right;">
                <button style="margin-top:15px;" (click)="sendMessage(msg.value)" class="btn btn-success">Send</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div id="list_people">
            <h2>Users in lobby</h2>
            <div id="people"></div>
            <h2>Games to join</h2>
            <div id="games"></div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./lobby.component.css'],
  providers: [Service]
})

export class LobbyComponent implements OnDestroy{
  private socket;
  private interval;

	constructor(private service: Service) {
    const username = sessionStorage.getItem('username');
  	if(username == null){
			window.location.href = '/';
      return;
		}

    this.service.getMessages().subscribe((data) =>{
      const messages = document.getElementById("messages");

      data.forEach(function(e){
        const date = new Date(parseInt(e.timestamp));
        const hours = date.getHours();
        const minutes = "0" + date.getMinutes();
        const seconds = "0" + date.getSeconds();
        const time = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

        messages.innerHTML += '<h4>' + time + '</h4><p class="a_message"><span>' + e.username + "</span> : " + e.message + "</p>";
      });
        
    });

    this.socket = io();

    this.socket.emit("user", username);

    this.socket.emit("getUsers"); //Asked to get users
    this.socket.emit("getGames"); //Asked to get users
    this.interval = setInterval((function(self) { //Asked to get users and games every 5s
      return function() {
         self.socket.emit("getUsers");
         self.socket.emit("getGames");
      }
    })(this),5000);

    this.socket.on('getUsers', function(data){
      const users = document.getElementById("people");

      var html = "";

      data.forEach(function(e){
          html += '<p class="a_user">' + e +"</p>";
      });

      users.innerHTML = html;
    });

    this.socket.on('getGames', function(data){
      const games = document.getElementById("games");

      var html = "";

      data.forEach(function(e){
          html += '<p class="a_game"><button class="btn btn-primary" routerLink="/pregame">' + e +"</button></p>";
      });

      games.innerHTML = html;
    });

    this.socket.on('messageFromServer', function(data){
      const messages = document.getElementById("messages");
      const date = new Date(parseInt(data.timestamp));
      const hours = date.getHours();
      const minutes = "0" + date.getMinutes();
      const seconds = "0" + date.getSeconds();
      const time = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
      messages.innerHTML += '<h4>' + time + '</h4><p class="a_message"><span>' + data.username + "</span> : " + data.message + "</p>";
    });
	}

  ngOnDestroy() {
    clearInterval(this.interval);
    this.socket.disconnect();
  }

  sendMessage(msg: string){
    this.socket.emit("message", {
        message: msg,
        username: sessionStorage.getItem('username')
    });
    const input = (<HTMLInputElement>document.getElementById("msg"));
    input.value = "";
  }
}