import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { sha256 } from 'js-sha256';

@Injectable()
export class Service {
  	constructor (private http: Http) {}

	checkUsername(username: string) {
		let name = encodeURIComponent(username);

		return this.http.get('/User/CheckUsername/' + name).map((res:Response) => res.json());
	}

	registerUser(username: string, password: string) {
		let name = encodeURIComponent(username);
		let pw = sha256(encodeURIComponent(password));

		return this.http.get('/User/Register/' + name + '/' +pw).map((res:Response) => res.json());
	}

 	loginUser(username: string, password: string) {
		let name = encodeURIComponent(username);
		let pw = sha256(encodeURIComponent(password));

	    return this.http.get('/User/Login/' + name + '/' +pw).map((res:Response) => res.json());
    }

 	updateUsername(currentUsername: string, username: string) {
		let name = encodeURIComponent(username);

		return this.http.get('/User/Edit/' + currentUsername + '/' + name).map((res:Response) => res.json());

    }

 	updateUser(currentUsername: string, username: string, password: string) {
		let name = encodeURIComponent(username);
		let pw = sha256(encodeURIComponent(password));
		return this.http.get('/User/Edit/' + currentUsername + '/' + name + '/' +pw).map((res:Response) => res.json());
    }

    getMessages() {
		return this.http.get('/Message/All').map((res:Response) => res.json());
    }
}