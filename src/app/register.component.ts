import { Component } from '@angular/core';
import { User } from './user';
import { Service } from './service';

@Component({
  selector: 'register',
  template: `
    <div class="container">
        <h1>Register form</h1>
        <form #registerForm="ngForm">
          <div class="form-group">
            <label for="username">Username</label>
            <input [class.notValid]="!validUsername" [class.valid]="validUsername" type="text" class="form-control" id="username" name="username" [(ngModel)]="model.username">
          </div>
     
          <div class="form-group">
            <label for="password">Password</label>
            <input [class.notValid]="!validPw" [class.valid]="validPw" type="password" class="form-control" name="password "id="password" [(ngModel)]="model.password">
          </div>
     
          <button type="submit" class="btn btn-success" (click)="registerUser()">Submit</button>
        </form>
    </div>
  `,
  styleUrls: ['./register.component.css'],
  providers: [Service]
})

export class RegisterComponent {
	constructor(private registerService: Service) {
	  	if(sessionStorage.getItem('username') != null){
			window.location.href = '/lobby';
		}
	}


	model = new User("","");

	validUsername = true;
	validPw = true;

	registerUser() {
		this.validUsername = true;
		this.validPw = true;

		if(this.model.password == ""){
			this.validPw = false;
		}

		if(this.model.username == ""){
			this.validUsername = false;
		}
		else{
			this.registerService.checkUsername(this.model.username).subscribe((data) =>{
				if(!data.valid){
					this.validUsername = false;
				}
				else{
					if(this.validPw){ //Everything is ok, we can register
						this.registerService.registerUser(this.model.username, this.model.password).subscribe((data) =>{
							if("username" in data){
								sessionStorage.setItem('username', data.username);
								window.location.href = '/lobby';
							}
						});
					}
				}
			});
		}
	}
}