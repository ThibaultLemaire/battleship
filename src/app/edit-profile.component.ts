import { Component } from '@angular/core';
import { User } from './user';
import { Service } from './service';

@Component({
  selector: 'edit-profile',
  template: `
    <div class="container">
        <h1>Edit profile</h1>
        <form #registerForm="ngForm">
          <div class="form-group">
            <label for="username">Username</label>
            <input [class.notValid]="!validUsername" [class.valid]="validUsername" type="text" class="form-control" id="username" name="username" [(ngModel)]="model.username">
          </div>
     
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password "id="password" [(ngModel)]="model.password">
          </div>
     
          <button type="submit" class="btn btn-success" (click)="updateUser()">Submit</button>
        </form>
    </div>
  `,
  styleUrls: ['./edit-profile.component.css'],
  providers: [Service]
})

export class EditProfileComponent {
	constructor(private registerService: Service) {
	  	if(sessionStorage.getItem('username') == null){
			window.location.href = '/';
		}
	}


	model = new User(sessionStorage.getItem('username'),"");

	validUsername = true;

	updateUser() {
		this.validUsername = true;

		if(this.model.username == ""){
			this.validUsername = false;
		}
		else{
			this.registerService.checkUsername(this.model.username).subscribe((data) =>{
				if(!data.valid && sessionStorage.getItem('username') != this.model.username){
					this.validUsername = false;
				}
				else{
					if(this.model.password != ""){
						this.registerService.updateUser(sessionStorage.getItem('username'), this.model.username, this.model.password).subscribe((data) =>{
							if("username" in data){
								sessionStorage.removeItem('username');
								sessionStorage.setItem('username', data.username);
								window.location.href = '/lobby';
							}
						});
					}
					else{
						this.registerService.updateUsername(sessionStorage.getItem('username'), this.model.username).subscribe((data) =>{
							if("username" in data){
								sessionStorage.removeItem('username');
								sessionStorage.setItem('username', data.username);
								window.location.href = '/lobby';
							}
						});
					}
				}
			});
		}
	}
}