import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	isConnected = false;

	constructor() {
	  	if(sessionStorage.getItem('username') != null){
			this.isConnected = true;
		}
  	}

	disconnect(){
		sessionStorage.removeItem('username');
	  	window.location.href = "/";
	}
}
