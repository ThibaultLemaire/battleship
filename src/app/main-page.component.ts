import { Component } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'main-page-component',
  template: `
    <div class="container">
      <div id="image">
        <p id="title">Battleship : A crazy game with ships</p>
        <img class="img-responsive" src="assets/sea.jpg" alt="Battleship" />
      </div>
    </div>
  `,
  styleUrls: ['./main-page.component.css'],
  providers: []
})

export class MainPageComponent {
	constructor() {
	}
}