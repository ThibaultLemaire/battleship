import { Injectable } from '@angular/core';
import { GridCoordinates } from "./gridcoordinates";

@Injectable()
export class GameService {
    get occupiedCells(): Array<GridCoordinates> {
        return JSON.parse(sessionStorage.getItem('positions'));
    }
    set occupiedCells(value: Array<GridCoordinates>) {
        sessionStorage.setItem('positions', JSON.stringify(value));
    }

    constructor () {

    }
}