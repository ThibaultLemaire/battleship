import { Component, OnDestroy } from '@angular/core';
import { Ship } from "./ship";
import { GridCoordinates } from "./gridcoordinates";
import { GameService } from "./game.service";
import * as io from "socket.io-client";

@Component({
  selector: 'game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
  providers: [GameService]
})

export class GameComponent implements OnDestroy{

	private static cellClass = "gridSquare";
	private static rowClass = "gridRow";
	private socket;

	get occupiedCells(): Array<GridCoordinates> {
		return this.service.occupiedCells;
	}
	set occupiedCells(value: Array<GridCoordinates>) {
		this.service.occupiedCells = value;
	}

	constructor(private service: GameService) {
	  	if(sessionStorage.getItem('username') == null){
			window.location.href = '/';
			return;
		}

		var socket = io();
		this.socket = socket;

		socket.emit("game", sessionStorage.getItem('username'));

		var occCells = this.occupiedCells; //occupieds cells

	    socket.on('cellReceived', function(data){
			console.log('cellReceived', data)
	    	if(data in occCells){
	    		data.valid = true;
	    	}
	    	else{
				data.valid = false;
	    	}
	    	socket.emit("cellToSend", data);
	    });

	    var gettingACell = this.getCell;

	    socket.on('cellToSend', function(data){
			console.log('cellToSend', data);
	    	const coo = new GridCoordinates(data.x,data.y);
	    	const cell = gettingACell(coo);
	    	if(data.valid){
	    		cell.style.backgroundColor = "green";
	    		const i = occCells.indexOf(coo);
    			occCells.splice(i, 1);
	    	}
	    	else{
				cell.style.backgroundColor = "red";
	    	}

	    	if(occCells.length == 0){
	    		//this.socket.emit();
	    		console.log("You LOSE !");
	    	}
	    });

		console.log("occupied cells:", this.occupiedCells);
	}

	getCell(coordinates: GridCoordinates){
		var row = document.getElementsByClassName(GameComponent.rowClass + ' ' + coordinates.y)[0] as HTMLDivElement;
		var cell = row.getElementsByClassName(GameComponent.cellClass + ' ' + coordinates.x)[0] as HTMLDivElement;
		return cell;
	}

	onGridClick(clickedCell: HTMLElement) {
		var coordinates = this.getCoordinates(clickedCell);

		this.socket.emit("cellToTest", coordinates);
	}

	getCoordinates(cell: HTMLElement) : GridCoordinates {
		var x = cell.className.replace(GameComponent.cellClass + ' ', '');
		var y = cell.parentElement.className.replace(GameComponent.rowClass + ' ', '');
		return new GridCoordinates(parseInt(x), parseInt(y));
	}

	ngOnDestroy(){
		this.socket.disconnect();
	}
}