export class GridCoordinates {
	constructor(
	    public x: number,
	    public y: number
	  ) {  }
}