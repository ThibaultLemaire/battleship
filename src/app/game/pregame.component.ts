import { Component, ViewChildren } from '@angular/core';
import { Ship } from "./ship";
import { GridCoordinates } from "./gridcoordinates";
import { GameService } from "./game.service";
import * as io from "socket.io-client";

@Component({
  selector: 'game',
  templateUrl: './pregame.component.html',
  styleUrls: ['./pregame.component.css'],
  providers: [GameService]
})

export class PreGameComponent{

	private static cellClass = "gridSquare";
	private static rowClass = "gridRow";
	@ViewChildren('ships')
	private ships;
	private socket;

	isHorizontal = true;
	chosenShip: number = null;
	get occupiedCells(): Array<GridCoordinates> {
		return this.service.occupiedCells;
	}
	set occupiedCells(value: Array<GridCoordinates>) {
		this.service.occupiedCells = value;
	}

	private availableShips = [
		new Ship("Carrier", 5),
		new Ship("Battleship", 4),
		new Ship("Battleship", 4),
		new Ship("Submarine", 3),
		new Ship("Destroyer", 2),
		new Ship("Destroyer", 2),
		new Ship("Destroyer", 2)]

	constructor(private service: GameService) {
	  	if(sessionStorage.getItem('username') == null){
			window.location.href = '/';
		}
		this.occupiedCells = [];
	}

	onGridClick(clickedCell: HTMLElement) {
		if (this.chosenShip == null) {
			return;
		}
		var selectedShip = this.availableShips[this.chosenShip];
		var coordinates = this.getCoordinates(clickedCell);
		var coordinatesRange = this.cellRange(coordinates, selectedShip.length, this.isHorizontal);
		var isPositionOK = this.checkPositioning(coordinatesRange, this.occupiedCells);
		if (isPositionOK) {
			coordinatesRange.forEach(pos => {
				this.getCell(pos).style.backgroundColor = "#ca0000";
			});
			this.occupiedCells = this.occupiedCells.concat(coordinatesRange);
			this.availableShips.splice(this.chosenShip, 1);
			this.chosenShip = null;
		}
	}

	onShipPick(index: number) {
		this.chosenShip = index;

		const toColor = index + 2;

		const size = this.ships._results[0].nativeElement.children.length;

		for(var i=2;i<size;i++){
			this.ships._results[0].nativeElement.children[i].style.backgroundColor = "#f0f0f0";
		}

		this.ships._results[0].nativeElement.children[toColor].style.backgroundColor = "#acacac";
	}

	onOrientationChange() {
		this.isHorizontal = !this.isHorizontal;
		
		document.getElementById("orientationSwitch").innerHTML = this.isHorizontal ? "Horizontal" : "Vertical";
	}

	getCoordinates(cell: HTMLElement) : GridCoordinates {
		var x = cell.className.replace(PreGameComponent.cellClass + ' ', '');
		var y = cell.parentElement.className.replace(PreGameComponent.rowClass + ' ', '');
		return new GridCoordinates(parseInt(x), parseInt(y));
	}

	getCell(coordinates: GridCoordinates){
		var row = document.getElementsByClassName(PreGameComponent.rowClass + ' ' + coordinates.y)[0] as HTMLDivElement;
		var cell = row.getElementsByClassName(PreGameComponent.cellClass + ' ' + coordinates.x)[0] as HTMLDivElement;
		return cell;
	}
	
	cellRange(coord: GridCoordinates, length: number, isHorizontal: boolean): Array<GridCoordinates>{
		var cellRange: Array<GridCoordinates> = [];
		if (isHorizontal) {
			for (var i = 0; i < length; i++) {
				cellRange.push(new GridCoordinates(coord.x + i, coord.y));
			}
		} else {
			for (var i = 0; i < length; i++) {
				cellRange.push(new GridCoordinates(coord.x, coord.y + i));
			}
		}
		return cellRange;
	}

	checkPositioning(wantedPositions: Array<GridCoordinates>, occupiedPositions: Array<GridCoordinates>): boolean {
		var isCorrect = true;
		wantedPositions.forEach(wPos => {
			if (wPos.x < 9 && wPos.y < 9) {
				occupiedPositions.forEach(oPos => {
				if (wPos.x == oPos.x && wPos.y == oPos.y) {
					isCorrect = false;
				}
			});
			} else {
				isCorrect = false;
			}
		});
		return isCorrect;
	}
	startGame(){
		window.location.href = "/game";
	}
}