import { Component } from '@angular/core';
import { User } from './user';
import { Service } from './service';

@Component({
  selector: 'login',
  template: `
    <div class="container">
        <h1>Login form</h1>
        <form #loginForm="ngForm">
          <div class="form-group">
            <label for="username">Username</label>
            <input [class.notValid]="!validUsername" [class.valid]="validUsername" type="text" class="form-control" id="username" name="username" [(ngModel)]="model.username">
          </div>
     
          <div class="form-group">
            <label for="password">Password</label>
            <input [class.notValid]="!validPw" [class.valid]="validPw" type="password" class="form-control" name="password "id="password" [(ngModel)]="model.password">
          </div>
     
          <button type="submit" class="btn btn-success" (click)="loginUser()">Submit</button>
        </form>
    </div>
  `,
  styleUrls: ['./login.component.css'],
  providers: [Service]
})

export class LoginComponent {
	constructor(private registerService: Service) {
	  	if(sessionStorage.getItem('username') != null){
			window.location.href = '/lobby';
		}
	}

	model = new User("","");

	validUsername = true;
	validPw = true;


	loginUser() {
		this.validUsername = true;
		this.validPw = true;
		let ok = true;

		if(this.model.password == ""){
			this.validPw = false;
			ok = false;
		}

		if(this.model.username == ""){
			this.validUsername = false;
			ok = false;
		}

		if(ok){
			this.registerService.loginUser(this.model.username, this.model.password).subscribe((data) =>{
				if("username" in data){ //login correct
					sessionStorage.setItem('username', data.username);
					window.location.href = '/lobby';
				}
				else{
					this.validUsername = false;
					this.validPw = false;
				}
			});
		}
	}
}