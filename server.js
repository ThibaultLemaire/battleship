var express = require('express');
var path = require('path');
var app = express();
var server  = require('http').createServer(app);
var io = require('socket.io').listen(server);

var port = 3000;
var mongoDBPort = 27017;

var MongoClient = require('mongodb').MongoClient;
var MongoUrl = 'mongodb://localhost:' + mongoDBPort + '/Battleship';
var users = [];
var games = [];

/*var ObjectID = require('mongodb').ObjectID;
var obj_id = new ObjectID('593f9d69031b9419443aeb47');*/

app.use(express.static(path.join(__dirname, 'dist')));

/*const url = require('url');    
app.get('/category', function(req, res) {
    res.redirect(url.format({
       pathname:"/",
       query: {
          "a": 1,
          "b": 2,
          "valid":"your string here"
        }
     }));
 });*/

io.on('connection', function(socket){
    
  var user = "";
  var cells = "";
  var game = "";

	socket.on('message', function(msg){

		var timestamp =  Date.now().toString();

		msg.timestamp = timestamp;

		MongoClient.connect(MongoUrl, function(err, db) {
			if(err){
			  return;
			}
			else{
			  var collection = db.collection("Messages");

			  collection.insertOne({
			    "username" : msg.username,
			    "message" : msg.message,
			    "timestamp" : msg.timestamp
			  });

			  return;
			}
		});

		io.sockets.emit('messageFromServer', msg);
	});

	socket.on('user', function(data){
    user = data;
		users.push(data);
	});

  socket.on('disconnect', function(){
    var i = users.indexOf(user);
    users.splice(i, 1);

    if(game != ""){
      i = games.indexOf(game);
      games.splice(i, 1);
    }
  });

	socket.on('getUsers', function(){
		io.sockets.emit('getUsers', users);
	});

  socket.on('getGames', function(){
    io.sockets.emit('getGames', games);
  });

  socket.on('game', function(data){
    game = data;
    games.push(data);
  });

  // game
  socket.on('cellToTest', function(data) {
    socket.broadcast.emit('cellReceived', data);
  });

  socket.on('cellToSend', function(data) {
    socket.broadcast.emit('cellToSend', data);
  });
});

app.get('/register', function(req, res, next) {
	next();
});

app.get('/User/CheckUsername/:username', function(req,res){
  res.setHeader('Content-Type', 'application/json');

  MongoClient.connect(MongoUrl, function(err, db) {
    if(err){
      return res.end(JSON.stringify({ valid: false}));
    }
    else{
      var collection = db.collection("Users");

      collection.findOne({username: req.params.username}, function(err, docs) {
        if(err){
          return res.end(JSON.stringify({ valid: false}));
        }
        if(docs == null){
          return res.end(JSON.stringify({ valid: true}));
        }
        return res.end(JSON.stringify({ valid: false}));
      });
    }

    /*var col = db.collection('Test');
    var cursor = col.find();

    cursor.each(function(err, item) {
        if(item == null) {
            db.close();
            return;
        }
        console.log(item._id);
    });

    col.insertOne({
      "address" : "31000 Toulouse"
    });*/
  });
});

app.get('/User/Login/:username/:password', function(req,res){

  res.setHeader('Content-Type', 'application/json');

  MongoClient.connect(MongoUrl, function(err, db) {
    if(err){
      return res.end(JSON.stringify({}));
    }
    else{
      var collection = db.collection("Users");

      collection.findOne({username: req.params.username, password: req.params.password}, function(err, docs) {
        if(err){
          return res.end(JSON.stringify({}));
        }
        if(docs == null){
          return res.end(JSON.stringify({}));
        }
        return res.end(JSON.stringify({username: docs.username}));
      });
    }
  });
});



app.get('/User/Register/:username/:password', function(req,res){
  //var from = req.headers.referer;

  res.setHeader('Content-Type', 'application/json');

  MongoClient.connect(MongoUrl, function(err, db) {
    if(err){
      return res.end();
    }
    else{
      var collection = db.collection("Users");

      collection.insertOne({
        "username" : req.params.username,
        "password" : req.params.password
      });
      return res.end(JSON.stringify({username: req.params.username}));
    }
  });
});

app.get('/User/Edit/:oldUsername/:username', function(req,res){
  //var from = req.headers.referer;

  res.setHeader('Content-Type', 'application/json');

  MongoClient.connect(MongoUrl, function(err, db) {
    if(err){
      return res.end();
    }
    else{
      var collection = db.collection("Users");

      collection.update({"username": req.params.oldUsername},{
      	$set: {
       		"username" : req.params.username
     	}
      });
      return res.end(JSON.stringify({username: req.params.username}));
    }
  });
});

app.get('/User/Edit/:oldUsername/:username/:password', function(req,res){
  //var from = req.headers.referer;

  res.setHeader('Content-Type', 'application/json');

  MongoClient.connect(MongoUrl, function(err, db) {
    if(err){
      return res.end();
    }
    else{
      var collection = db.collection("Users");

      collection.update({"username": req.params.oldUsername},{
        "username" : req.params.username,
        "password" : req.params.password
      });
      return res.end(JSON.stringify({username: req.params.username}));
    }
  });
});

app.get('/Message/All', function(req,res){

  res.setHeader('Content-Type', 'application/json');

  MongoClient.connect(MongoUrl, function(err, db) {
    if(err){
      return res.end(JSON.stringify({}));
    }
    else{
      	var collection = db.collection("Messages");
		var cursor = collection.find();
		var messages = [];

		// Execute the each command, triggers for each document
		cursor.each(function(err, item) {
		    if(item == null) {
		        db.close(); // you may not want to close the DB if you have more code....
		        return res.end(JSON.stringify(messages));
		    }
		    messages.push(item);
		});
    }
  });
});

app.get('/*', function(req, res, next) {
  res.sendFile(__dirname + '/dist/index.html');
});

server.listen(port, function () {
  console.log('Listening on port ' + port + ' !');
})
