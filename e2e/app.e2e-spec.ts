import { BattleshipPage } from './app.po';

describe('battleship App', () => {
  let page: BattleshipPage;

  beforeEach(() => {
    page = new BattleshipPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
